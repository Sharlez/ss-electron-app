# Sunday Sauna Electron App

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Packaging](#packaging)

## Installation


```bash
npm install
```
## Usage

Launch Sunday Sauna app on your local machine and then
```bash
npm start
```
## Packaging
To create a distributable app

```bash
npm run make
```